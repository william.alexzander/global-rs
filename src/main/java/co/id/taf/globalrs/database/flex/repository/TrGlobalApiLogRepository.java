package co.id.taf.globalrs.database.flex.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import co.id.taf.globalrs.database.flex.entity.TrGlobalApiLogEntity;

@Repository
public interface TrGlobalApiLogRepository extends JpaRepository<TrGlobalApiLogEntity, Long> {

	@Query(value = "SELECT * FROM tr_global_api_log WHERE DATEADD (d , ?1, tr_global_api_log.DTM_CRT) < CURRENT_TIMESTAMP", nativeQuery = true)
	List<TrGlobalApiLogEntity> findAllByCreatedDateAfterParameter(Integer day);

	@Query(value = "DELETE FROM tr_global_api_log WHERE DATEADD (d , ?1, tr_global_api_log.DTM_CRT) < CURRENT_TIMESTAMP", nativeQuery = true)
	@Modifying
	@Transactional
	void deleteAllByCreatedDateAfterParameter(Integer day);


}
