package co.id.taf.globalrs.database.flex.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "tr_global_api_log")
public class TrGlobalApiLogEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "LOG_ID")
	@JsonProperty("LOG_ID")
	private Long log;

	@Column(name = "REQUEST")
	@JsonProperty("REQUEST")
	private String request;

	@Column(name = "DTM_CRT", updatable = false)
	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@JsonProperty("DTM_CRT")
	@JsonFormat(pattern = "yyyy/MM/dd HH:mm:ss", timezone = "Asia/Bangkok")
	private Date dateCreated;

	@Column(name = "STATUS", length = 10)
	@JsonProperty("STATUS")
	private String status;

	@Column(name = "SP_ID")
	@JsonProperty("SP_ID")
	private Long spId;

	@Column(name = "STORED_PROCEDURE")
	@JsonProperty("STORED_PROCEDURE")
	private String storedProcedure;

	public Long getLog() {
		return log;
	}

	public void setLog(Long log) {
		this.log = log;
	}

	public String getRequest() {
		return request;
	}

	public void setRequest(String request) {
		this.request = request;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getSpId() {
		return spId;
	}

	public void setSpId(Long spId) {
		this.spId = spId;
	}

	public String getStoredProcedure() {
		return storedProcedure;
	}

	public void setStoredProcedure(String storedProcedure) {
		this.storedProcedure = storedProcedure;
	}
}
