package co.id.taf.globalrs.database.flex.entity;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "M_GLOBAL_API")
@Getter
@Setter
@NoArgsConstructor
public class GlobalApiEntity implements Serializable  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "DB_NAME")
    private String dbName;

    @Column(name = "STORED_PROCEDURE")
    private String storedProcedure;

}
