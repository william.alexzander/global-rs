package co.id.taf.globalrs.database.flex.repository;


import co.id.taf.globalrs.database.flex.entity.GlobalApiEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface FlexGlobalApiRepository extends JpaRepository<GlobalApiEntity, Long> {

//    @Query("SELECT api FROM GlobalApiEntity api")
//    List<GlobalApiDto> fetchAll();

    @Query("SELECT api FROM GlobalApiEntity api")
    List<GlobalApiEntity> fetchAll();
}
