package co.id.taf.globalrs.database.flex.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
@Table(name = "M_USER_MOBILE")
@Getter
@Setter
@NoArgsConstructor
public class UserMobileEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "USER_ID")
    private Long id;

    @Column(name = "CUST_ID")
    private Long custId;

    @Column(name = "CUST_NO")
    private String custNo;

    @Column(name = "CUST_NAME")
    private String custName;

    @Column(name = "MOBILE_PHONE_NO", unique = true)
    private String phone;

    @Column(unique = true)
    private String email;

    @Column(name = "PLAYER_ID")
    private String playerId;
    
    @Column(name = "GENDER")
    private String gender;

    @Column(name = "IMG_PROFILE_FILENAME")
    private String imgProfileFilename;

    @Column(name = "IMG_PROFILE_FILEPATH")
    private String imgProfileFilepath;
    
    @JsonIgnore
    @JsonProperty(access = Access.WRITE_ONLY)
    @Column(name = "OTP")
    private String otp;

    @JsonIgnore
    @Column(name = "OTP_EXPIRE_DATE")
    private LocalDateTime otpExpireDate;

    @JsonIgnore
    @Column(name = "PASSWORD_EXPIRE_DATE")	
	private Date passwordExpireDate;

    @Column(name = "ID_CARD_NO", length = 100, unique = true)
    private String idCardNo;

    @Column(name = "BIRTH_DT")
    private LocalDate birthDate;

    @Column(name = "BIRTH_PLACE")
    private String birthPlace;

    @Column(name = "ACCESS_CODE")
    private String accessCode;

    @Column(name = "IS_VERIFY", length = 1)
    private Integer isVerify = 0;

    @Column(name = "IS_ACTIVE", length = 1)
    private Integer isActive = 0;
    
    @Column(name = "IS_LOGIN", length = 1)
    private Integer isLogin = 0;
    
    @Column(name = "IS_MIGRATION", length = 1)
    private Integer isMigration;

    @Column(name = "DEVICE_ID", length = 100)
    private String deviceId;

    @Column(name = "REFERRAL_CODE", unique = true)
    private String referralCode;

    @Column(name = "BRAND_NAME")
    private String brandName;

    @Column(name = "BRAND_MODEL")
    private String brandModel;

    @Column(name = "OS_TYPE")
    private String osType;

    @Column(name = "OS_VERSION")
    private String osVersion;

    @Column(name = "EMAIL_DIGISIGN")
    private String emailDigisign;    

    @Column(name = "VERIFY_EMAIL")
    private String verifyEmail;
    
    @Column(name = "USR_CRT", updatable = false)
    private String createdBy;
    
    @Column(name = "USR_UPD")
    private String updatedBy;

    @Column(name = "LOCK_COUNTER")
    private Integer lockCounter;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm")
    @Column(name = "LOCK_TIME")
    private LocalDateTime lockTime;
    
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm")
    @CreatedDate
    @Column(name = "DTM_CRT", updatable = false)
    private LocalDateTime dateCreated;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm")
    @LastModifiedDate
    @Column(name = "DTM_UPD")
    private LocalDateTime dateUpdated;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm")
    @LastModifiedDate
    @Column(name = "LAST_LOGIN_DATE")
    private LocalDateTime lastLoginDate;

    @JsonIgnore
    @Column(name = "OTP_REQUEST_DATE")
    private LocalDateTime otpRequestDate;

    @Column(name = "OTP_COUNTER", length = 1)
    private Integer otpCounter;

    @JsonIgnore
    @Column(name = "RESEND_OTP_REQUEST_DATE")
    private LocalDateTime resendOtpRequestDate;

    @Column(name = "RESEND_OTP_COUNTER", length = 1)
    private Integer resendOtpCounter;

    @JsonIgnore
    @Column(name = "LOGIN_OTP_REQUEST_DATE")
    private LocalDateTime loginOtpRequestDate;

    @Column(name = "LOGIN_OTP_COUNTER", length = 1)
    private Integer loginOtpCounter;

    @JsonIgnore
    @Column(name = "FORGOT_OTP_REQUEST_DATE")
    private LocalDateTime forgotOtpRequestDate;

    @Column(name = "FORGOT_OTP_COUNTER", length = 1)
    private Integer forgotOtpCounter;

    @Transient
    private String statusOtp;

    @Column(name = "HAS_SYNC", length = 1)
    private Integer hasSync;

    @Column(name = "HAS_NOTIFY_SYNC", length = 1)
    private Integer hasNotifySync;

    @Column(name = "USER_UUID")
    private String userUuid;

    @Column(name = "IS_CONSENT")
    private Integer isConsent;

}
