package co.id.taf.globalrs.database.flex.repository;

import co.id.taf.globalrs.dto.CurrentUser;
import co.id.taf.globalrs.database.flex.entity.UserMobileEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<UserMobileEntity, Long> {

    Optional<UserMobileEntity> findFirstByEmail(String email);
    
    @Query("SELECT user FROM UserMobileEntity user WHERE user.isVerify = 1  AND user.accessCode = :accessCode")
    Optional<UserMobileEntity> findFirstByAccessCode(String accessCode);
    
    @Query("SELECT user FROM UserMobileEntity user WHERE user.isVerify = 1  AND user.phone = :phone")
    Optional<UserMobileEntity> findFirstByPhone(String phone);

    @Query("SELECT new co.id.taf.globalrs.dto.CurrentUser(user.custName, user.email, user.phone) FROM UserMobileEntity user WHERE user.email = :login")
    Optional<CurrentUser> getCurrentUserByLogin(String login);

    @Query("SELECT COUNT(user) FROM UserMobileEntity user WHERE user.isVerify <> 0 AND user.phone = :phone AND user.idCardNo = :identityNumber")
    int phoneHasBeenRegistered(String phone, String identityNumber);

    @Query("SELECT COUNT(user) FROM UserMobileEntity user WHERE user.isVerify <> 0 AND user.email = :email AND user.idCardNo = :idCardNo")
    int emailHasBeenRegistered(String email, String idCardNo);


    @Query("SELECT COUNT(user) FROM UserMobileEntity user WHERE user.isVerify = 1 AND user.idCardNo = :idNumber")
    int userRegistered(String idNumber);

    Optional<UserMobileEntity> findFirstByIdCardNo(String idCardNo);

    @Query(name = "SELECT TOP(1) * FROM M_USER_MOBILE user WHERE M_USER_MOBILE = ?2 AND ID_CARD_NO = ?1", nativeQuery = true)
    Optional<UserMobileEntity> findFirstByIdCardNoAndPhone(String idCardNo, String phone);

    @Query("SELECT user FROM UserMobileEntity user WHERE user.isVerify = 0 AND user.phone = :phone AND user.idCardNo = :idCardNo")
    Optional<UserMobileEntity> findUnregisteredUser(String phone, String idCardNo);
    
    @Query("SELECT user FROM UserMobileEntity user WHERE user.isVerify = 1 AND user.phone = :phone AND user.idCardNo = :idCardNo")
    Optional<UserMobileEntity> findRegisteredUser(String phone, String idCardNo);


    @Query("SELECT user FROM UserMobileEntity user WHERE user.isVerify <> 0 AND (user.email = :email OR user.idCardNo = :idCardNo OR user.phone = :phone) ")
    List<UserMobileEntity> checkUserRegistered(String email, String idCardNo, String phone);

    @Query("SELECT user FROM UserMobileEntity user WHERE user.isVerify <> 0 AND user.id <> :id AND (user.email = :email OR user.idCardNo = :idCardNo OR user.phone = :phone OR user.accessCode = :accessCode) ")
    List<UserMobileEntity> checkUserEditProfile(Long id, String email, String idCardNo, String phone, String accessCode);
    
    @Query("SELECT user FROM UserMobileEntity user WHERE user.isVerify = 1  AND user.custId = :custId")
    Optional<UserMobileEntity> findActiveCustId(Long custId);

    @Query("SELECT user FROM UserMobileEntity user WHERE user.custId IS NULL ORDER BY user.dateCreated DESC")
    List<UserMobileEntity> findAllByCustIdIsNull();

    int countByReferralCode(String referralCode);
       
    @Transactional
    @Modifying
    @Query("UPDATE UserMobileEntity e SET e.playerId = '' WHERE e.playerId = :playerId AND e.id != :userId")
    void updateDuplicatePlayerId(String playerId, Long userId);
        
    @Query(value = "SELECT TOP(1000) * FROM M_USER_MOBILE WHERE USR_CRT = ?1 AND USR_UPD IS NULL ", nativeQuery = true)
    List<UserMobileEntity> getListUserMigration(String name);

    @Query(value = "SELECT TOP(1000) * FROM M_USER_MOBILE", nativeQuery = true)
    List<UserMobileEntity> getListAllUser();

    @Query("SELECT user.custNo FROM UserMobileEntity user WHERE user.isVerify = 1 AND user.custId = :custId")
    String loadCustNo(Long custId);

    @Query("SELECT user FROM UserMobileEntity user WHERE user.phone = :phone")
    List<UserMobileEntity> findListUserByPhone(String phone);
    
    @Query("SELECT user FROM UserMobileEntity user WHERE user.emailDigisign = :email AND user.id=:id AND user.isVerify = 1")
    Optional<UserMobileEntity> findUserByEmailDigisign(String email, Long id);

    @Query("SELECT user FROM UserMobileEntity user WHERE user.referralCode = :referralCode")
    Optional<UserMobileEntity> findByReferralCode(String referralCode);

    @Query("SELECT user FROM UserMobileEntity user WHERE user.email = :email AND user.phone = :phone AND user.isVerify = 1 AND user.playerId != ''")
    UserMobileEntity findFirstByEmailAndMobilePhoneNo(String email, String phone);
}
