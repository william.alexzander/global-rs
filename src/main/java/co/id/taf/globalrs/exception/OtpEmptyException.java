package co.id.taf.globalrs.exception;

import org.springframework.security.core.AuthenticationException;

public class OtpEmptyException extends AuthenticationException {

    public OtpEmptyException(String msg) {
        super(msg);
    }

    public OtpEmptyException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
