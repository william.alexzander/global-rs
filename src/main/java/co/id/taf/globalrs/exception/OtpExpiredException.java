package co.id.taf.globalrs.exception;

import org.springframework.security.core.AuthenticationException;

public class OtpExpiredException extends AuthenticationException {

    public OtpExpiredException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public OtpExpiredException(String msg) {
        super(msg);
    }
}
