package co.id.taf.globalrs.exception;


import org.springframework.security.core.AuthenticationException;

public class OtpMismatchException extends AuthenticationException {

    public OtpMismatchException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public OtpMismatchException(String msg) {
        super(msg);
    }
}
