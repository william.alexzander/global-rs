package co.id.taf.globalrs.props;

import lombok.Getter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;

@ConfigurationProperties(prefix = "jwt")
@ConstructorBinding
@Getter
public class JwtProps {
    private final String base64Secret;
    private final String header;
    private final long tokenValidityInSecondsForRememberMe;
    private final long tokenValidityInSeconds;
    private final long expiration;
    private final String routeAuthenticationPath;
    private final String routeAuthenticationRefresh;

        public JwtProps(String base64Secret, String header, long tokenValidityInSecondsForRememberMe, long tokenValidityInSeconds, long expiration, String routeAuthenticationPath, String routeAuthenticationRefresh) {
        this.base64Secret = base64Secret;
        this.header = header;
        this.tokenValidityInSecondsForRememberMe = tokenValidityInSecondsForRememberMe;
        this.tokenValidityInSeconds = tokenValidityInSeconds;
        this.expiration = expiration;
        this.routeAuthenticationPath = routeAuthenticationPath;
        this.routeAuthenticationRefresh = routeAuthenticationRefresh;
    }

    public long getTokenValidityInMilliSeconds() {
        return tokenValidityInSeconds * 1000;
    }

    public long getTokenValidityForRememberMeInMilliseconds() {
        return tokenValidityInSecondsForRememberMe * 1000;
    }
}
