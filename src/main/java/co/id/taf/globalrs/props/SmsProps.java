package co.id.taf.globalrs.props;

import lombok.Getter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;

@ConfigurationProperties(prefix = "sms.otp")
@ConstructorBinding
@Getter
public class SmsProps {

    private final long timeoutInMinutes;

    private final String secret;

    private final String url;

    public SmsProps(long timeoutInMinutes, String secret, String url) {
        this.timeoutInMinutes = timeoutInMinutes;
        this.secret = secret;
        this.url = url;
    }
}
