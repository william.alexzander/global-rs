package co.id.taf.globalrs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("test")
public class TestController {

    @Autowired
    PasswordEncoder encoder;

    @GetMapping
    public String encoder(Model model) {
//        model.addAttribute("content", new EmailContent(1L, "ASUN", "ASUN", "ASUN", "ASUN", "ASUN", "ASUN", "ASUN"));
        return "emailTemplate";
    }
}
