package co.id.taf.globalrs.service.impl;

import co.id.taf.globalrs.database.flex.entity.GlobalApiEntity;
import co.id.taf.globalrs.database.flex.repository.FlexGlobalApiRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Service;

import javax.persistence.PersistenceException;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.sql.Types;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

@Service
public class GlobalApiServiceFlex {

    private FlexGlobalApiRepository globalApiRepo;

    private static final Logger logger = LogManager.getLogger(MethodHandles.lookup().lookupClass());

    public GlobalApiServiceFlex(FlexGlobalApiRepository globalApiRepo) {
        this.globalApiRepo = globalApiRepo;
    }

    public List<GlobalApiEntity> allApi() {
        return globalApiRepo.fetchAll();
    }

    public void deleteGlobalApiLog() {
    }
}
