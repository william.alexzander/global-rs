package co.id.taf.globalrs.service.impl;

import co.id.taf.globalrs.database.flex.entity.GlobalApiEntity;
import co.id.taf.globalrs.database.flex.repository.FlexGlobalApiRepository;
import co.id.taf.globalrs.database.flex.entity.TrGlobalApiLogEntity;
import co.id.taf.globalrs.database.flex.repository.TrGlobalApiLogRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Service;

import java.lang.invoke.MethodHandles;
import java.sql.Types;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
public class GlobalApiService {

    private static final Logger logger = LogManager.getLogger(MethodHandles.lookup().lookupClass());

    @Autowired
    @Qualifier("jdbcTemplateFlex")
    private JdbcTemplate jdbcTemplateFlex;

    @Autowired
    @Qualifier("jdbcTemplateDigitaf")
    private JdbcTemplate jdbcTemplateDigitaf;

    @Autowired
    @Qualifier("jdbcTemplateClaris")
    private JdbcTemplate jdbcTemplateClaris;

    private FlexGlobalApiRepository globalApiRepo;

    @Autowired
    private TrGlobalApiLogRepository trGlobalApiLogRepo;

    public GlobalApiService(FlexGlobalApiRepository globalApiRepo) {
        this.globalApiRepo = globalApiRepo;
    }

    public Map<String, Object> execStoredProcedure(Long id, Map<String, Object> params) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String formattedDateTime = LocalDateTime.now().format(formatter);
        Map<String, Object> result = new HashMap<>();
        result.put("RESPONSE_TIME", formattedDateTime);
        result.put("STATUS", "SUCCES");
        Optional<GlobalApiEntity> entity = globalApiRepo.findById(id);
        TrGlobalApiLogEntity trGlobalApiLog = new TrGlobalApiLogEntity();
        trGlobalApiLog.setRequest(params.toString());
        trGlobalApiLog.setStatus("SUCCES");
        trGlobalApiLog.setSpId(id);
        try {
            if (entity.isPresent()) {
                System.out.println("SP Name : " + entity.get().getStoredProcedure());

                JdbcTemplate selectedJdbcTemplate = selectJdbcTemplate(entity.get().getDbName());
                SimpleJdbcCall jdbcCall = new SimpleJdbcCall(selectedJdbcTemplate)
                        .withProcedureName(entity.get().getStoredProcedure());

                params.forEach((key, value) -> jdbcCall.addDeclaredParameter(new SqlParameter(key, getSqlType(value))));
                result.put("DESCIPTION", jdbcCall.execute(params).get("#result-set-1"));
                trGlobalApiLog.setStoredProcedure(entity.get().getStoredProcedure());
            } else {
                result.put("DESCIPTION", null);
            }
        } catch (NullPointerException e){
            e.printStackTrace();
            result.put("STATUS", "ERROR");
            result.put("DESCIPTION", null);
            trGlobalApiLog.setStatus("ERROR");
            trGlobalApiLog.setStoredProcedure(entity.get().getStoredProcedure());
        } catch (Exception e) {
            e.printStackTrace();
            result.put("STATUS", "ERROR");
            trGlobalApiLog.setStatus("ERROR");
            trGlobalApiLog.setStoredProcedure(entity.get().getStoredProcedure());
            if (e.getMessage().contains("Unable to determine the correct call signature - no procedure/function/signature")) {
                result.put("DESCIPTION", entity.get().getStoredProcedure() + " procedure not found from " + entity.get().getDbName() + " database");
            } else {
                result.put("DESCIPTION", e.getMessage());
            }
        }
        trGlobalApiLogRepo.save(trGlobalApiLog);
        return result;
    }

    private int getSqlType(Object value) {
        if (value instanceof Integer) {
            return Types.INTEGER;
        } else if (value instanceof String) {
            return Types.VARCHAR;
        } else if (value instanceof Boolean) {
            return Types.BOOLEAN;
        } else if (value instanceof Double) {
            return Types.DOUBLE;
        }
        // Add more types as needed
        throw new IllegalArgumentException("Unsupported parameter type: " + value.getClass().getName());
    }

    private JdbcTemplate selectJdbcTemplate(String dbName) {
        switch (dbName) {
            case "flex":
                return jdbcTemplateFlex;
            case "deal":
                return jdbcTemplateDigitaf;
            case "claris":
                return jdbcTemplateClaris;
            default:
                return jdbcTemplateFlex;
        }
    }
}
