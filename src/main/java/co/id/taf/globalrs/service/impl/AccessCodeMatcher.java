package co.id.taf.globalrs.service.impl;

import co.id.taf.globalrs.config.dto.CustomUser;
import org.apache.log4j.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class AccessCodeMatcher {

    private static final Logger LOG = Logger.getLogger(AccessCodeMatcher.class);
    
    private final PasswordEncoder encoder;

    public AccessCodeMatcher(PasswordEncoder encoder) {
        this.encoder = encoder;
    }

    /*
     *  Will always false if user is not loggedIn
     */
    public boolean isAccessCodeMatchLoggedIn(String rawAccessCode) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        try {
            CustomUser user = (CustomUser) auth.getPrincipal();
            return encoder.matches(rawAccessCode, user.getPassword());
        } catch (NullPointerException e) {
            LOG.warn("User is not authenticated: {}", e);
            return false;
        } catch (ClassCastException e) {
            LOG.error("Class Cast exception thrown: {}", e);
            return false;
        }
    }

    public PasswordEncoder getEncoder() {
        return encoder;
    }
}
