package co.id.taf.globalrs.dto.hibernate;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class FAQDto {

    private Long id;

    private String moduleName;

}
