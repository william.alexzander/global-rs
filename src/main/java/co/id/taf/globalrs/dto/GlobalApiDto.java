package co.id.taf.globalrs.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class GlobalApiDto {

    private Long id;

    private String apiName;

    private String note;

}
