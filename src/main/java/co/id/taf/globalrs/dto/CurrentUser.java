package co.id.taf.globalrs.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CurrentUser {

    private String name;
    private String username;
    private String phone;
}
