package co.id.taf.globalrs.controller;


import co.id.taf.globalrs.database.flex.entity.GlobalApiEntity;
import co.id.taf.globalrs.service.impl.GlobalApiService;
import co.id.taf.globalrs.utils.ConfinsAuthKey;
import com.google.gson.Gson;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.text.ParseException;
import java.util.Map;

@RestController
@RequestMapping("globalapi")
public class GlobalApiControler {

    private GlobalApiService globalApiSvc;

    private GlobalApiEntity global;

    public GlobalApiControler(GlobalApiService globalApiSvc) {
        this.globalApiSvc = globalApiSvc;
    }

    private static final Logger log = Logger.getLogger(GlobalApiControler.class);

    @Value(value = "${authorization.nologin.auth.key}")
    private String authorizationKey;

    @PostMapping()
//    @RequestMapping(value = "Global", method = RequestMethod.POST, produces = "application/json")
    public Map<String, Object> globalApi(@RequestParam(value = "Global", required = true) Long id, @RequestBody(required = true) Map<String, Object> requestBody) throws IOException, ParseException {
        System.out.println("Request id = " + id);

        System.out.println("Received dynamic request body: " + requestBody);

        return globalApiSvc.execStoredProcedure(id, requestBody);
    }

}