package co.id.taf.globalrs.controller;


import co.id.taf.globalrs.database.flex.entity.GlobalApiEntity;
import co.id.taf.globalrs.response.BaseResponse;
import co.id.taf.globalrs.response.MasterDataResDto;
import co.id.taf.globalrs.service.impl.GlobalApiServiceFlex;
import co.id.taf.globalrs.utils.ConfinsAuthKey;
import com.google.gson.Gson;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.NoSuchAlgorithmException;
import java.util.List;

@RestController
@RequestMapping("globalapi")
public class GlobalControlerFlex {

    private GlobalApiServiceFlex globalApiSvc;

    private GlobalApiEntity global;

    public GlobalControlerFlex(GlobalApiServiceFlex globalApiSvc) {
        this.globalApiSvc = globalApiSvc;
    }

    private static final Logger log = Logger.getLogger(GlobalControlerFlex.class);

    @Value(value = "${authorization.nologin.auth.key}")
    private String authorizationKey;


    @GetMapping("/flex")
    public ResponseEntity<BaseResponse<List<GlobalApiEntity>>> apiFlex(@RequestHeader("Authorization") String authorization) {
        try {
            String key = ConfinsAuthKey.encodeConfins(authorizationKey);
            if(authorization.equals(key)) {
                log.warn(new Gson().toJson(globalApiSvc.allApi()));
                return ResponseEntity.ok(new BaseResponse<>(MasterDataResDto.defaultSuccess(), globalApiSvc.allApi()));
            }else {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
            }
        }catch(NoSuchAlgorithmException e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new BaseResponse<>("500",e.getMessage(), null));
        }
    }

}