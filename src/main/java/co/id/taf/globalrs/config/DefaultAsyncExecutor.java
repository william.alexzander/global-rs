package co.id.taf.globalrs.config;

import org.springframework.context.annotation.Bean;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class DefaultAsyncExecutor {

    @Bean
    public Executor defaultExecutor() {
        return Executors.newFixedThreadPool(100);
    }
}
