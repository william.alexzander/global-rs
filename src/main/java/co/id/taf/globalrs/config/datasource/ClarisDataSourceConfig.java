package co.id.taf.globalrs.config.datasource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "sqlServerEntityManagerFactoryClaris",
        transactionManagerRef = "sqlServerTransactionManagerClaris",
        basePackages = "co.id.taf.globalrs.database.Claris.repository")
public class ClarisDataSourceConfig {


    @Bean
    @ConfigurationProperties(prefix = "spring.third-datasource")
    public DataSourceProperties sqlServerDataSourcePropertiesClaris() {
        return new DataSourceProperties();
    }
    @Bean
    public DataSource sqlServerDataSourceClaris(@Qualifier("sqlServerDataSourcePropertiesClaris") DataSourceProperties dataSourceProperties) {
        return dataSourceProperties.initializeDataSourceBuilder().build();
    }

    @Bean(name = "sqlServerEntityManagerFactoryClaris")
    public LocalContainerEntityManagerFactoryBean sqlServerEntityManagerFactoryClaris(@Qualifier("sqlServerDataSourceClaris") DataSource sqlServerDataSourceClaris, EntityManagerFactoryBuilder builder) {

        return builder.dataSource(sqlServerDataSourceClaris)
                .packages("co.id.taf.globalrs.database.Claris")
                .persistenceUnit("sqlserver")
                .build();

    }

    @Bean
    public PlatformTransactionManager sqlServerTransactionManagerClaris(@Qualifier("sqlServerEntityManagerFactoryClaris")
                                                                  EntityManagerFactory factory) {
        return new JpaTransactionManager(factory);
    }

    @Bean
    public JdbcTemplate jdbcTemplateClaris(@Qualifier("sqlServerDataSourceClaris") DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }
}