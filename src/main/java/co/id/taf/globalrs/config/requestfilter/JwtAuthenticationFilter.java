package co.id.taf.globalrs.config.requestfilter;

import java.io.IOException;
import java.util.*;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.GenericFilterBean;

import co.id.taf.globalrs.config.TokenProvider;

public class JwtAuthenticationFilter extends GenericFilterBean {
    
    private static final Logger LOG = Logger.getLogger(JwtAuthenticationFilter.class);

    public static final String AUTHORIZATION_HEADER = "Authorization";

    private TokenProvider tokenProvider;

    public JwtAuthenticationFilter(TokenProvider tokenProvider) {
        this.tokenProvider = tokenProvider;
    }

    Map<String, Long> bannedTokens = new HashMap<>();

    private static final Set<String> EXCLUDED_URIS = new HashSet<>(Arrays.asList(
            "/api/v1/info/banner/header",
            "/api/v1/info/banner/detail",
            "/monitoring",
            "/api/v1/promo/banner/header",
            "/api/v1/promo/banner/detail",
            "/flex_uploads/profile",
            "/api/v1/document",
            "/api/v1/inbox/notif/",
            "/flex_uploads/",
            "/payment-channel/setting",
            "/api/v1/e-statement",
            "/api/v1/banner-popup"
    ));
    private static final String EXEMPT_JWT = "obziqe0q9bUMWUldS3jiIA5Z+lqfB8OsrvLEKAFEMrE=";

    private boolean isUriExcluded(String requestURI, String jwtToken) {
        return EXCLUDED_URIS.stream().noneMatch(requestURI::contains) && !EXEMPT_JWT.equals(jwtToken);
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {

        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        String jwt = resolveToken(httpServletRequest);
        String requestURI = httpServletRequest.getRequestURI();

        removeExpiredTokens();
        if (isUriExcluded(requestURI, jwt) && bannedTokens.containsKey(jwt)) {
            HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;
            httpServletResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED); // 401 status if expired tokennya
            httpServletResponse.setContentType("application/json");
            String jsonResponse = "{\"status\": \"401\", \"error\": \"Unauthorized\", \"message\": \"Your session has been expired\"}";
            httpServletResponse.getWriter().write(jsonResponse);
            return;
        }

        if (requestURI.equals("/flex/api/v1/user/logout")) {
            if (!bannedTokens.containsKey(jwt)) {
                bannedTokens.put(jwt, System.currentTimeMillis());
            }
        }

        if (StringUtils.hasText(jwt) && tokenProvider.validateToken(jwt)) {
            Authentication authentication = tokenProvider.getAuthentication(jwt);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            // LOG.debug("set Authentication to security context for '"+authentication.getName()+"', uri: "+ requestURI);
        } else {
            // LOG.debug("no valid JWT token found, uri: "+ requestURI);
        }

        filterChain.doFilter(servletRequest, servletResponse);
    }

    void removeExpiredTokens() {
        // long expirationTime = 10 * 1000; // 10 second untuk testing
        long expirationTime = 24 * 60 * 60 * 1000; // 24 hours sesuai expired token
        long currentTime = System.currentTimeMillis();
        bannedTokens.entrySet().removeIf(entry -> currentTime - entry.getValue() > expirationTime);
    }

    private String resolveToken(HttpServletRequest request) {
        String bearerToken = request.getHeader(AUTHORIZATION_HEADER);
        if (StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer ")) {
            return bearerToken.substring(7);
        } else {
            return bearerToken;
        }
    }
}
