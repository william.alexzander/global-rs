package co.id.taf.globalrs.config.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CustomUser {

    private final String username;
    
    private final String password;

    private final Long custId;

    private final String fullName;
    
    private final Long userId;


    public CustomUser(String username, String password, Long custId, String fullName, Long userId) {
        this.username = username;
        this.password = password;
        this.custId = custId;
        this.fullName = fullName;
        this.userId = userId;
    }

    @Override
    public String toString() {
        return this.username;
    }
}
