package co.id.taf.globalrs.config.datasource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;


import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "sqlServerEntityManagerFactoryDigitaf",
        transactionManagerRef = "sqlServerTransactionManagerDigitaf",
        basePackages = "co.id.taf.globalrs.database.digitaf.repository")
public class DigitafDataSourceConfig {

    @Primary
    @Bean
    @ConfigurationProperties(prefix = "spring.second-datasource")
    public DataSourceProperties sqlServerDataSourcePropertiesDigitaf() {
        return new DataSourceProperties();
    }
    @Primary
    @Bean
    public DataSource sqlServerDataSourceDigitaf(@Qualifier("sqlServerDataSourcePropertiesDigitaf") DataSourceProperties dataSourceProperties) {
        return dataSourceProperties.initializeDataSourceBuilder().build();
    }

    @Primary
    @Bean(name = "sqlServerEntityManagerFactoryDigitaf")
    public LocalContainerEntityManagerFactoryBean sqlServerEntityManagerFactoryDigitaf(@Qualifier("sqlServerDataSourceDigitaf") DataSource sqlServerDataSourceDigitaf, EntityManagerFactoryBuilder builder) {

        return builder.dataSource(sqlServerDataSourceDigitaf)
                .packages("co.id.taf.globalrs.database.digitaf")
                .persistenceUnit("sqlserver")
                .build();

    }

    @Primary
    @Bean
    public PlatformTransactionManager sqlServerTransactionManagerDigitaf(@Qualifier("sqlServerEntityManagerFactoryDigitaf")
                                                                  EntityManagerFactory factory) {
        return new JpaTransactionManager(factory);
    }

    @Bean
    public JdbcTemplate jdbcTemplateDigitaf(@Qualifier("sqlServerDataSourceDigitaf") DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }
}