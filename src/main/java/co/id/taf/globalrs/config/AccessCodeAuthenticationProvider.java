package co.id.taf.globalrs.config;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Optional;

import co.id.taf.globalrs.database.flex.entity.UserMobileEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import co.id.taf.globalrs.config.dto.CustomUser;
import co.id.taf.globalrs.exception.OtpEmptyException;
import co.id.taf.globalrs.exception.OtpExpiredException;
import co.id.taf.globalrs.exception.OtpMismatchException;
import co.id.taf.globalrs.database.flex.repository.UserRepository;
import co.id.taf.globalrs.service.impl.AccessCodeMatcher;

@Service
@Qualifier("accessCodeAuthenticationProvider")
public class AccessCodeAuthenticationProvider implements AuthenticationProvider {

    private final UserRepository repo;

    @Autowired
    private AccessCodeMatcher matcher;

    public AccessCodeAuthenticationProvider(UserRepository repo) {
        this.repo = repo;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String username = authentication.getName();
        String rawAccessCode = authentication.getCredentials().toString();

        Optional<UserMobileEntity> findUser = repo.findFirstByPhone(username);

        if(findUser.isPresent()) {
            UserMobileEntity user = findUser.get();
            if(matcher.getEncoder().matches(rawAccessCode, user.getAccessCode())) {
                CustomUser principal = new CustomUser(username, user.getAccessCode(), user.getCustId(), user.getCustName(), user.getId());
                return new UsernamePasswordAuthenticationToken(principal, user.getId(), new ArrayList<>());
            }
        }
        throw new UsernameNotFoundException("User not found for: " + username);
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.equals(UsernamePasswordAuthenticationToken.class);
    }

    private boolean isOtpAvailable(UserMobileEntity user) throws OtpEmptyException {
        if(user.getOtp() != null) {
            return true;
        }
        System.out.println("DOESN'T HAVE OTP");

        throw new OtpEmptyException("No available otp for: " + user.getAccessCode());
    }

    private boolean isOtpNotExpired(UserMobileEntity user) throws OtpExpiredException {
        System.out.println("OTP IS EXPIRED "+user.getOtpExpireDate().toString());
        if(user.getOtpExpireDate().isAfter(LocalDateTime.now())) {
            return true;
        }
        System.out.println("OTP IS EXPIRED");

        throw new OtpExpiredException("OTP is expired for: " + user.getAccessCode());
    }

    private boolean isOtpMatch(UserMobileEntity user, String otp) throws OtpMismatchException {
        if(user.getOtp().equals(otp)) {
            return true;
        }
        System.out.println("OTP IS NOT MATCH");
        throw new OtpMismatchException("OTP mismatch for: " + user.getAccessCode());
    }
}
