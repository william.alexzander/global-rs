package co.id.taf.globalrs.config;

import java.security.Key;
import java.util.ArrayList;
import java.util.Date;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import co.id.taf.globalrs.config.dto.CustomUser;
import co.id.taf.globalrs.props.JwtProps;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.UnsupportedJwtException;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;

@Component
public class TokenProvider implements InitializingBean {    
    
    private static final Logger log = Logger.getLogger(TokenProvider.class);

    private JwtProps jwtConfig;

    private static final String AUTHORITIES_KEY = "auth";
    
    private static final String JWT_SUBJECT_DELIMITER = "%flex%";

    private Key key;
    
    public TokenProvider(JwtProps jwtConfig) {
        this.jwtConfig = jwtConfig;
//        this.userRepo = userRepo;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        byte[] keyBytes = Decoders.BASE64.decode(jwtConfig.getBase64Secret());
        this.key = Keys.hmacShaKeyFor(keyBytes);
    }

    public String createToken(Authentication auth, boolean rememberMe) {
        String authorities = "";

        long now = (new Date()).getTime();
        Date validity = new Date(now + (rememberMe ? this.jwtConfig.getTokenValidityForRememberMeInMilliseconds() : this.jwtConfig.getTokenValidityInMilliSeconds()));

        CustomUser user = (CustomUser) auth.getPrincipal();

        String nonNullJwtSubject = String.join(JWT_SUBJECT_DELIMITER, buildJwtSubject(user.getUsername(), user.getPassword(),user.getCustId(), user.getFullName(), user.getUserId()));

        return Jwts.builder()
                .setSubject(nonNullJwtSubject)
//                .claim("user_id", "x")
                .claim(AUTHORITIES_KEY, authorities)
                .signWith(key, SignatureAlgorithm.HS512)
                .setExpiration(validity)
                .compact();
    }

    public Authentication getAuthentication(String token) {
        Claims claims = Jwts.parserBuilder()
                .setSigningKey(key)
                .build()
                .parseClaimsJws(token)
                .getBody();

        String subject = claims.getSubject().toString();
        String[] principal = subject.split(JWT_SUBJECT_DELIMITER.toString(), 0);

        Long custId = principal[2].equals("-") ? null : Long.parseLong(principal[2]);

        CustomUser user = new CustomUser(principal[0], principal[1], custId, principal[3], Long.parseLong(principal[4]));

        return new UsernamePasswordAuthenticationToken(user, token, new ArrayList<>());
    }

    public boolean validateToken(String authToken) {
        try {
            Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(authToken);
            return true;
        } catch (io.jsonwebtoken.security.SecurityException | MalformedJwtException e) {
//            log.info("Invalid JWT signature.");
            log.trace("Invalid JWT signature trace: {}", e);
        } catch (ExpiredJwtException e) {
            log.info("Expired JWT token.");
            log.trace("Expired JWT token trace: {}", e);
        } catch (UnsupportedJwtException e) {
            log.info("Unsupported JWT token.");
            log.trace("Unsupported JWT token trace: {}", e);
        } catch (IllegalArgumentException e) {
            log.info("JWT token compact of handler are invalid.");
            log.trace("JWT token compact of handler are invalid trace: {}", e);
        }
        return false;
    }
    
    public static String[] buildJwtSubject(String username, String password, Long custId, String name, Long userId) {
        String[] nonNullJwtSubject = new String[5];
        nonNullJwtSubject[0] = username;
        nonNullJwtSubject[1] = password;
        nonNullJwtSubject[2] = custId == null ? "-" : String.valueOf(custId);
        nonNullJwtSubject[3] = name;
        nonNullJwtSubject[4] = String.valueOf(userId);
        return nonNullJwtSubject;
    }

}
