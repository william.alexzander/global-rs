package co.id.taf.globalrs.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@Configuration
@EnableJpaAuditing(auditorAwareRef = "auditorAware")
public class AuditorAwareConfig {

    private AuditorAware<String> auditorAware;

    public AuditorAwareConfig(@Qualifier("customAuditorAwareImpl") AuditorAware<String> auditorAware) {
        this.auditorAware = auditorAware;
    }

    @Bean
    public AuditorAware<String> auditorAware() {
        return this.auditorAware;
    }


}

