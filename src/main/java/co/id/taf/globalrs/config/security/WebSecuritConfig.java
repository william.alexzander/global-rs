package co.id.taf.globalrs.config.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.HttpStatusEntryPoint;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.filter.CorsFilter;

import co.id.taf.globalrs.config.TokenProvider;
import co.id.taf.globalrs.config.security.jwt.JwtConfigurer;
import co.id.taf.globalrs.config.security.jwt.JwtDeniedHandler;

//@Configuration
//@EnableWebSecurity
@EnableScheduling
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class WebSecuritConfig extends WebSecurityConfigurerAdapter {

    private CorsFilter corsFilter;
    private JwtDeniedHandler deniedHandler;
    private TokenProvider provider;
    private AuthenticationProvider authProvider;

    private final HttpStatusEntryPoint http401EntryPoint = new HttpStatusEntryPoint(HttpStatus.UNAUTHORIZED);

    public WebSecuritConfig(CorsFilter corsFilter, JwtDeniedHandler deniedHandler, TokenProvider provider, AuthenticationProvider authProvider) {
        this.corsFilter = corsFilter;
        this.deniedHandler = deniedHandler;
        this.provider = provider;
        this.authProvider = authProvider;
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring()
                .antMatchers(HttpMethod.OPTIONS, "/**")

                // allow anonymous resource requests
                .antMatchers(
                        "/*.html",
                        "/favicon.ico",
                        "/**/*.html",
                        "/**/*.css",
                        "/**/*.js",
                        "/h2-console/**"
                );
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .addFilterBefore(corsFilter, UsernamePasswordAuthenticationFilter.class)
                .exceptionHandling()
                .authenticationEntryPoint(http401EntryPoint)
                .accessDeniedHandler(deniedHandler)

                .and()
                .headers()
                .frameOptions()
                .sameOrigin()

                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)

                .and()
                .authorizeRequests()
                .antMatchers("/", "/globalapi/**").permitAll()
                .anyRequest().authenticated()
                .and()
                .apply(securityConfigurerAdapter());

    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(this.authProvider);
    }

    private JwtConfigurer securityConfigurerAdapter() {
        return new JwtConfigurer(provider);
    }
}
