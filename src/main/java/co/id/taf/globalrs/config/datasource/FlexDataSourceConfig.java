package co.id.taf.globalrs.config.datasource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;


import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "sqlServerEntityManagerFactoryFlex",
        transactionManagerRef = "sqlServerTransactionManagerFlex",
        basePackages = "co.id.taf.globalrs.database.flex.repository")
public class FlexDataSourceConfig {


    @Bean
    @ConfigurationProperties(prefix = "spring.datasource")
    public DataSourceProperties sqlServerDataSourcePropertiesFlex() {
        return new DataSourceProperties();
    }
    @Bean
    public DataSource sqlServerDataSourceFlex(@Qualifier("sqlServerDataSourcePropertiesFlex") DataSourceProperties dataSourceProperties) {
        return dataSourceProperties.initializeDataSourceBuilder().build();
    }

    @Bean
    @ConfigurationProperties("spring.datasource")
    public DataSource secondaryDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "sqlServerEntityManagerFactoryFlex")
    public LocalContainerEntityManagerFactoryBean sqlServerEntityManagerFactoryFlex(@Qualifier("sqlServerDataSourceFlex") DataSource sqlServerDataSourceFlex, EntityManagerFactoryBuilder builder) {

        return builder.dataSource(sqlServerDataSourceFlex)
                .packages("co.id.taf.globalrs.database.flex")
                .persistenceUnit("sqlserver")
                .build();

    }

    @Bean
    public PlatformTransactionManager sqlServerTransactionManagerFlex(@Qualifier("sqlServerEntityManagerFactoryFlex")
                                                                  EntityManagerFactory factory) {
        return new JpaTransactionManager(factory);
    }

    @Bean
    public JdbcTemplate jdbcTemplateFlex(@Qualifier("sqlServerDataSourceFlex") DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }
}