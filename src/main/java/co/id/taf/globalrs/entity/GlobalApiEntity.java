package co.id.taf.globalrs.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "M_GLOBAL_API")
@Getter
@Setter
@NoArgsConstructor
public class GlobalApiEntity implements Serializable  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "API_NAME")
    private String apiName;

    @Column(name = "NOTE")
    private String note;

}
