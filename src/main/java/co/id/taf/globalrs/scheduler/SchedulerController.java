package co.id.taf.globalrs.scheduler;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import javax.annotation.PostConstruct;

import co.id.taf.globalrs.database.flex.repository.*;
import co.id.taf.globalrs.service.impl.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RestController;

@Component
@RestController
public class SchedulerController {
	private static final Logger LOG = Logger.getLogger(SchedulerController.class);
	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
	
	@Autowired
	private GlobalApiService globalApiService;

	@Autowired
	private TrGlobalApiLogRepository trGlobalApiLogRepo;

	@Value("${schedular.globalapi.log.cleanup.day}")
	private String globalApiLogDayCleanup;

	private double postContruct = 0;

	@PostConstruct
	public void init() {
		postContruct = Math.random();
		LOG.info("Controller has been initialized.  " + postContruct);
	}

	@Scheduled(cron = "0 0 2 * * ?")
	public void globalApi() {
		try {
			String identifier = "SCHEDULER_" + postContruct + "_" +  Math.random();
			LOG.info("=============== LOG EXECUTE SCHEDULER CLEAN GLOBAL API LOG =================" + identifier);
			trGlobalApiLogRepo.deleteAllByCreatedDateAfterParameter(Integer.valueOf(globalApiLogDayCleanup));
		} catch (Exception e) {
			LOG.error(e.getMessage());
		}
	}
}
