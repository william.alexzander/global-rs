package co.id.taf.globalrs.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MasterDataResDto {
	private String status;
	private String message;

	public static MasterDataResDto defaultSuccess() {
		return new MasterDataResDto("200", "Success");
	}

	public static MasterDataResDto defaultNotFound() {
		return new MasterDataResDto("400", "Not found");
	}

	public static MasterDataResDto defaultUnauthorized() {
		return new MasterDataResDto("401", "Unauthorized");
	}
	
	public static MasterDataResDto defaultUnexpectedError() {
		return new MasterDataResDto("500", "Unexpected Error");
	}

	public static MasterDataResDto defaultBadRequest() {
		return new MasterDataResDto("400", "Bad Request");
	}

	public static MasterDataResDto defaultUnexpectedError(String message) {
		return new MasterDataResDto("500", message);
	}


	public static MasterDataResDto defaultSuccess(String message) {
		return new MasterDataResDto("200", message);
	}
}
