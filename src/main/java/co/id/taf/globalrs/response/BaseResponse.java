package co.id.taf.globalrs.response;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class BaseResponse<T> extends MasterDataResDto {

    private T data;

    public BaseResponse(String status, String message, T data) {
        super(status, message);
        this.data = data;
    }

    public BaseResponse(int statusInt, String message, T data) {
        super(String.valueOf(statusInt), message);
        this.data = data;
    }

    public BaseResponse(MasterDataResDto statusMessage, T data) {
        super(statusMessage.getStatus(), statusMessage.getMessage());
        this.data = data;
    }
    
    @SuppressWarnings("unchecked")
	public BaseResponse(MasterDataResDto statusMessage, String data) {
        super(statusMessage.getStatus(), statusMessage.getMessage());
        this.data = (T) data;
    }
}
